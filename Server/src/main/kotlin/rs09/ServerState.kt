package rs09

enum class ServerState {
    BOOTING,
    RUNNING,
    SHUTDOWN,
    CLOSED
}