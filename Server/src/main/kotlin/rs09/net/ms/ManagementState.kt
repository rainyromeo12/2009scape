package rs09.net.ms

enum class ManagementState {
    CONNECTING,
    CONNECTED,
    DISCONNECTING,
    DISCONNECTED
}